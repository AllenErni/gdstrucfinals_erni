#include <iostream>
#include <string>
#include "Queue.h"
#include "Stack.h"
#include <time.h>


using namespace std;

int main()
{
	int added;
	int size = 1;
	Stack<int> stack(size);
	Queue<int> queue(size);
	int quit = 0;
	int choice = 0;
	

	while (quit == 0)
	{
		cout << "Whatchu want to do?" << endl;
		cout << "[1] Push" << endl;
		cout << "[2] Pop" << endl;
		cout << "[3] Print all then delete" << endl;
		cin >> choice;

		// Push

		if (choice == 1)
		{
			cout << "Input number: ";
			cin >> added;
			stack.push(added);
			queue.push(added);
			cout << "Stack: " << stack.top() << endl;
			cout << "Queue: " << queue.top() << endl;
		}

		// Pop

		if (choice == 2)
		{

			stack.pop();
			queue.pop();

			cout << "Stack: " << stack.top() << endl;
			cout << "Queue: " << queue.top() << endl;
		}

		// Print 

		if (choice == 3)
		{
			cout << "Stack Elements: ";
			for (int i = 0; i < stack.getSize(); i++)
			{
				cout << stack[i] << " ";
			}
			cout << " " << endl;

			cout << "Queue elements: ";
			for (int i = 0; i < queue.getSize(); i++)
			{
				cout << queue[i] << " ";
			}
			cout << "" << endl;
			

			// Deleting


			if (stack.getSize() > 0)
			{
				while (stack.getSize() > 0)
				{
					stack.remove(0);
				}
			}

			if (queue.getSize() > 0)
			{
				while(queue.getSize() > 0)
				{
					queue.remove(0);
				}
			}
		}
		system("pause");
	}
	return 0;
}